def read_json(infile):
    """read class groups from JSON file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import json
    with open(infile, 'r') as f:
        class_groups = json.load(f)

    return class_groups


def read_csv(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import csv
    with open(infile, 'r') as f:
        c = csv.reader(f)
        groups = {}
        for row in c:
            if len(row) == 2:
                groups[row[0]] = row[1]
            elif len(row) == 3:
                groups[row[0]] = row[1] + ', ' + row[2]
            #print(',' .join(row))
        class_groups = groups

    return class_groups


def read_csv_numpy(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    from numpy import loadtxt
    # COMPLETE ME!!

    return class_groups

if __name__ == "__main__":
    d = read_csv('class_groups.csv')
    print(d)
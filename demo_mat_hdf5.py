def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return f

# to open matlab v7.3 files duse h5py
    #don't have to load everything at once
    # will need to map it to np.array() to be able to use it

def read_any_mat(infile):
    """read any .mat file

    :param infile: input file (str)
    :return: mat_dict
    """
    try:
        d = read_mat(infile)
    except NotImplementedError:
        f = read_hdf5(infile)
        d = dict(f)
    return d

if __name__ == "__main__":
    d = read_any_mat('mat_v73.mat')
    d = read_any_mat('mat_v5.mat')

